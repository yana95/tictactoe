import * as constants from './constants';

const newGameAction = () => ({
    type: constants.NEW_GAME
});
const gameOverAction = () => ({
    type: constants.GAMEOVER
});
const moveAction = (player, cellIndex) => ({
    type: constants.MOVE,
    payload: {player, cellIndex}
});
const changePlayerAction = () => ({
    type: constants.PLAYER,
});
const winnerAction = (player) => ({
    type: constants.WINNER,
    payload: {player}
});
const chooseGameModeAction = (isPlayWithComputer) => ({
    type: constants.PLAY_WITH_COMPUTER,
    payload: {isPlayWithComputer}
});
const newSettingsAction = (settings) => ({
    type: constants.NEW_SETTINGS,
    payload: settings
});

export {
    newGameAction,
    gameOverAction,
    moveAction,
    changePlayerAction,
    winnerAction,
    chooseGameModeAction,
};