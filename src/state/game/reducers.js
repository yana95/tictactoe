import { combineReducers } from 'redux';
import { playerO, playerX } from 'common/constants';
import * as constants from './constants';

const boardReducer = (state = [0,1,2,3,4,5,6,7,8], action) => {
    switch (action.type) {
        case constants.NEW_GAME:
            return [0,1,2,3,4,5,6,7,8];
        case constants.MOVE:
            let newBoard = state;
            newBoard[action.payload.cellIndex] = action.payload.player;
            return  newBoard;
        default:
            return state;
    }
};
const activePlayerReducer = (state = playerX, action ) => {
    switch (action.type) {
        case constants.PLAYER :
            return state === playerX ? playerO : playerX;
        case constants.NEW_GAME :
            return playerX;
        default:
            return state;
    }
};
const gameOverReducer = (state = false, action) => {
    switch (action.type){
        case constants.NEW_GAME:
            return false;
        case constants.WINNER:
            return true;
        case constants.GAMEOVER:
            return true;
        default:
            return state;
    }
};
const winnerReducer = (state = '', action) => {
    switch (action.type){
        case constants.NEW_GAME:
            return null;
        case constants.WINNER:
            return action.payload.player;
        default:
            return state;
    }
};
const gameModeReducer = (state = false, action) => {
    switch (action.type){
        case constants.PLAY_WITH_COMPUTER:
            return action.payload.isPlayWithComputer;
        default:
            return state;
    }
};

export default combineReducers({
    board: boardReducer,
    gameOver: gameOverReducer,
    winner: winnerReducer,
    activePlayer: activePlayerReducer,
    playWithComputer: gameModeReducer
});