const NEW_GAME = 'newGame';
const GAMEOVER = 'gameOver';
const MOVE = 'move';
const WINNER = 'winner';
const PLAYER = 'player';
const PLAY_WITH_COMPUTER = 'playWithComputer';
const NEW_SETTINGS = 'newSettings';

export {
    NEW_GAME,
    GAMEOVER,
    MOVE,
    PLAYER,
    WINNER,
    PLAY_WITH_COMPUTER,
    NEW_SETTINGS
};