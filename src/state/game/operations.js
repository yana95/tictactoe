import { newGameAction,
    gameOverAction,
    changePlayerAction,
    winnerAction,
    moveAction,
    chooseGameModeAction,
    chooseHumanChipAction
} from './actions';
import {isWinner, isFullBoard} from 'common/utils';
import { playerO, playerX } from 'common/constants';

const checkWinner = (board, player) => (dispatch) => {
    let hasWinner = true;
    if(isWinner(board, player)){
        dispatch(winnerAction(player));
        dispatch(gameOverAction());
    } else if (isFullBoard(board)){
        dispatch(winnerAction(''));
        dispatch(gameOverAction());
    } else {
        hasWinner = false;
    }
    return hasWinner;
};
const playerMakeAction = (player, cellIndex) => (dispatch) => {
    dispatch(moveAction(player, cellIndex));
    dispatch(changePlayerAction());
};
const startGameWithNewSettings = (settings) => (dispatch) => {
   dispatch(chooseGameModeAction(settings.isPlayWithComputer));
   dispatch(newGameAction());
};


export {
    checkWinner,
    playerMakeAction,
    newGameAction,
    startGameWithNewSettings
};