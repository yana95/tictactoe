import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import reducer from './game';

const configureStore = (state = {}) => {
    const rootReducer = reducer;
    const middleware = [thunk];
    const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
    const store = createStore(rootReducer,  state, composeEnhancers(
        applyMiddleware(...middleware),
    ));


    return store;
};

export {
    configureStore
};