import { playerO, playerX } from 'common/constants';

const isWinner = (board, player) => {
    if (
        (board[0] == player && board[1] == player && board[2] == player) ||
        (board[3] == player && board[4] == player && board[5] == player) ||
        (board[6] == player && board[7] == player && board[8] == player) ||
        (board[0] == player && board[3] == player && board[6] == player) ||
        (board[1] == player && board[4] == player && board[7] == player) ||
        (board[2] == player && board[5] == player && board[8] == player) ||
        (board[0] == player && board[4] == player && board[8] == player) ||
        (board[2] == player && board[4] == player && board[6] == player)
    ) {
        return true;
    } else {
        return false;
    }
};
const isFullBoard = (board) => {
    return  !board.filter(s => s !== playerO && s !== playerX).length;
};
const emptyIndexes = (board) => {
    return  board.filter(s => s !== playerO && s !== playerX);
};
const minimax = (newBoard, player ) => {
    const availSpots = emptyIndexes(newBoard);
    if (isWinner(newBoard, playerO)){
        return {score: -10};
    }
    else if (isWinner(newBoard, playerX)){
        return {score: 10};
    }
    else if (availSpots.length === 0){
        return {score:0};
    }
    const moves = [];
    for (let i = 0; i < availSpots.length; i++){
        const move = {};
        move.index = newBoard[availSpots[i]];
        newBoard[availSpots[i]] = player;
        if (player == playerX){
            const result = minimax(newBoard, playerO);
            move.score = result.score;
        }
        else{
            var result = minimax(newBoard, playerX);
            move.score = result.score;
        }
        newBoard[availSpots[i]] = move.index;
        moves.push(move);
    }
    let bestMove;
    if(player === playerX){
        let bestScore = -10000;
        for(let i = 0; i < moves.length; i++){
            if(moves[i].score > bestScore){
                bestScore = moves[i].score;
                bestMove = i;
            }
        }
    }else{
        let bestScore = 10000;
        for(let i = 0; i < moves.length; i++){
            if(moves[i].score < bestScore){
                bestScore = moves[i].score;
                bestMove = i;
            }
        }
    }
    return moves[bestMove];
};
export {
    isWinner,
    isFullBoard,
    minimax,
};