import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter, Route, Switch,  } from 'react-router-dom';

import { configureStore } from './state/store';
import App from 'containers/app/app';
import Game from 'containers/game/game';
import Start from 'containers/start/start';

const store = configureStore();
render(
    <Provider store={store}>
        <BrowserRouter>
            <App>
                <Switch>
                    <Route exact path='/' component = {Start} />
                    <Route path='/game' component = {Game} />
                </Switch>
            </App>
        </BrowserRouter>
    </Provider>,
    document.getElementById('app')
);

module.hot.accept();