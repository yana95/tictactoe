import React from 'react';
import './customButton.scss';

const CustomButton = ({onClick, icon}) => {
    return (
        <button onClick={onClick} className='custom-button'>
            <div className='icon-wrapper'>
                {icon}
            </div>
        </button>
    );
};
export default CustomButton;