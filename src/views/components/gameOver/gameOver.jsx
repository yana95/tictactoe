import React from 'react';
import Circle from 'components/circle/circle';
import Cross from 'components/cross/cross';
import { playerX } from 'common/constants';
import './gameOver.scss';

const GameOver = ({winner}) => {
    let icon;
    if(winner){
        icon = winner === playerX ? <Cross/> : <Circle/>;
    }
    return (
        <div className='game-over'>
            {
                winner
                    ? <div className='with-winner'>
                        {icon}
                        <span className='label'>wins!</span>
                    </div>
                    : <div className='label'>DRAW GAME</div>
            }
        </div>
    );
};
export default GameOver;