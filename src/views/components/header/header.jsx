import React from 'react';
import CustomButton from 'components/customButton/customButton';
import './header.scss';

const Header = ({onClickCancel, onClickRestart}) => {
    return (
        <div className='header'>
            <h1><span>Tic</span><span>Tac</span><span>Toe</span></h1>
            <div className='controls'>
                <div className='control-btn'>
                    <CustomButton
                        onClick={onClickRestart}
                        icon={<i className="fas fa-redo-alt"></i>}
                    />
                </div>
                <div className='control-btn'>
                    <CustomButton
                        onClick={onClickCancel}
                        icon={<i className="fas fa-sign-out-alt"></i>}
                    />
                </div>
            </div>
        </div>
    );
};
export default Header;