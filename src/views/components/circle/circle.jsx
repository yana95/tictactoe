import React from 'react';
import './circle.scss';

const Circle = () => {
    return (
        <div className='circle'>
            <i className="far fa-circle"></i>
        </div>
    );
};
export default Circle;