import React from 'react';
import { render } from 'react-dom';
import { playerO, playerX } from 'common/constants';
import Circle from 'components/circle/circle';
import Cross from 'components/cross/cross';
import './cell.scss';

const Cell = ({player, onClick}) => {
    let icon;
    if(player === playerX || player === playerO){
        icon = player === playerX ? <Cross/> : <Circle/>;
    }
    return (
        <button className={`cell ${player}`} onClick={onClick} >
            { icon }
        </button>
    );
};
export default Cell;