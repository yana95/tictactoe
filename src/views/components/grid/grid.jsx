import React from 'react';
import { render } from 'react-dom';
import Cell from './cell/cell'
import './grid.scss';

const Grid = ({board, onClickCell}) => {
    return (
        <div className='grid'>
            {
                board.map((item,i) => (
                    <Cell player={board[i]} key={new Date + i} onClick={() => onClickCell(i)} />
                ))
            }
        </div>
    );
};
export default Grid;