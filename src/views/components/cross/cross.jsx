import React from 'react';
import './cross.scss';

const Cross = () => {
    return (
        <div className='cross'>
            <i className='fas fa-times'></i>
        </div>
    );
};
export default Cross;