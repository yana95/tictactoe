import React from 'react';
import PropTypes from 'prop-types';
import { render } from 'react-dom';
import { connect } from 'react-redux'
import {withRouter } from 'react-router-dom';
import Header from 'components/header/header';
import 'reset-css';
import './app.scss';
import { gameOperations } from 'game';

class App extends React.Component {
    render(){
        return (
            <div className="main-container">
                <Header
                    onClickRestart={this.props.onClickNewGame}
                    onClickCancel={() => this.props.history.push('/')}
                />
                <div className='content'>
                    {this.props.children}
                </div>
            </div>
        );
    }
}
App.propTypes = {
    onClickNewGame: PropTypes.func,
    children: PropTypes.node,
};
App.defaultProps = {
    onClickNewGame: () => {},
    children: null,
};

const mapDispatchToProps = {
    onClickNewGame: gameOperations.newGameAction,
};

export default withRouter(connect(null, mapDispatchToProps)(App));

