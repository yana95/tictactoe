import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux'
import { gameOperations } from 'game';
import Grid from 'components/grid/grid';
import GameOver from 'components/gameOver/gameOver';
import { playerO, playerX } from 'common/constants';
import { minimax} from 'common/utils';

class Game extends Component {
    componentDidMount(){
        if(this.props.playWithComputer){
            this.aiMove();
        }
    }
    componentDidUpdate(prevProps) {
        if (this.props.board !== prevProps.board && this.props.playWithComputer) {
            this.aiMove();
        }
    }
    aiMove = () => {
        const bestMove = minimax(this.props.board, playerX);
        this.props.playerMakeAction(playerX, bestMove.index);
    };

    handlePlayerMoveAction = (cellIndex) => {
        const {board, activePlayer, playWithComputer, checkWinner, playerMakeAction } = this.props;
        if(board[cellIndex] !== playerO && board[cellIndex] !== playerX  ){
            const currentPlayer = activePlayer;
            playerMakeAction(currentPlayer, cellIndex);
            checkWinner(board, currentPlayer);
            if(playWithComputer){
                this.aiMove();
                checkWinner(board, playerX);
            }
        }
    };

    render(){
        return (
            <div>
                { this.props.gameOver && <GameOver winner={this.props.winner}/> }
                <Grid board={this.props.board}
                      onClickCell={this.handlePlayerMoveAction}
                      player={this.props.activePlayer}
                />
            </div>
        )
    }
}
Game.propTypes = {
    playWithComputer: PropTypes.bool,
    humanPlayer: PropTypes.string,
    board: PropTypes.array,
    activePlayer: PropTypes.string,
    gameOver: PropTypes.bool,
    winner: PropTypes.string,
    checkWinner: PropTypes.func,
    playerMakeAction: PropTypes.func,
    newGameAction: PropTypes.func,
};
Game.defaultProps = {
    playWithComputer: true,
    board: [],
    activePlayer: '',
    gameOver: false,
    winner: null,
    checkWinner: () => {},
    playerMakeAction: () => {},
    newGameAction: () => {},
};

const mapStateToProps = (state) => ({
    board: state.board,
    playWithComputer: state.playWithComputer,
    activePlayer: state.activePlayer,
    gameOver: state.gameOver,
    winner: state.winner
});
const mapDispatchToProps = {
    checkWinner: gameOperations.checkWinner,
    playerMakeAction: gameOperations.playerMakeAction,
    newGameAction: gameOperations.newGameAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(Game)