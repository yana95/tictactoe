import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux'
import { gameOperations } from 'game';
import { playerO, playerX } from 'common/constants';
import './start.scss';

class Start extends Component {
    state = {
        onePlayerMode: false,
    };

    onClickOnePlayerMode = () => {
        this.setState({onePlayerMode: true});
    };

    onClickTwoPlayerMode = () => {
        this.setState({onePlayerMode: false});
    };

    onClickStartGame = () => {
        this.props.startGameWithNewSettings({
            isPlayWithComputer: this.state.onePlayerMode
        });
        this.props.history.push('/game');
    };

    render(){
        return (
            <div className='start'>
                <h2>Choose game mode please</h2>
                <div className='modes'>
                    <div className='game-mode'>
                        <button
                            onClick={this.onClickOnePlayerMode}
                            className={this.state.onePlayerMode ? 'active big-button' : 'big-button'}
                        >
                            1 player
                        </button>
                        <p>Game against the computer</p>
                    </div>
                    <div className='game-mode'>
                        <button
                            onClick={this.onClickTwoPlayerMode}
                            className={!this.state.onePlayerMode ? 'active big-button' : 'big-button'}
                        >
                            2 players
                        </button>
                        <p>Game with friend</p>
                    </div>
                </div>
                <button onClick={this.onClickStartGame} className='big-button' >Start game</button>
            </div>
        );
    }
}

Start.propTypes = {
    startGameWithNewSettings: PropTypes.func,
};
Start.defaultProps = {
    startGameWithNewSettings: () => {},
};

const mapDispatchToProps = {
    startGameWithNewSettings: gameOperations.startGameWithNewSettings,
};

export default connect(null, mapDispatchToProps)(Start)